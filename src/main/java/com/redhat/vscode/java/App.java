package com.redhat.vscode.java;

import com.dneonline.calculator.Calculator;

public final class App {

    public static void main(String[] args) {
        
        System.out.println("10 + 20 = " + new Calculator().getCalculatorSoap().add(10, 20));
    }

}
